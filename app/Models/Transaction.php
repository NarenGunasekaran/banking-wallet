<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transaction';

    protected $fillable = [
        'id',
        'user_id',
        'type',
        'amount',
        'status',
        'reason',
        'name',
        'total_balance',
        'created_at',
        'updated_at'
    ];

    public function transaction_info() {
        return $this->hasMany(
            TransactionInfo::class
        );
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
