<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionInfo extends Model
{
    use HasFactory;

    protected $table = 'transaction_info';

    protected $fillable = [
        'id',
        'transaction_id',
        'pp1',
        'pp2',
        'pp3',
        'created_at',
        'updated_at'
    ];

    public function transaction() {
        return $this->belongsTo(Transaction::class);
    }
}
