<?php
namespace App\GraphQL\Types;

use App\Models\Transaction;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TransactionType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Transaction',
        'description' => 'Details about transaction',
        'model' => Transaction::class
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Id of the transaction',
            ],
            'user_id'=> [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Id of user',
            ],
            'type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The type of the transaction',
            ],
            'amount' => [
                'type' => Type::nonNull(Type::float()),
                'description' => 'Transaction amount',
            ],
            'status' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Transaction status',
            ],
            'reason' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Transaction reason',
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Transaction name',
            ],
            'total_balance' => [
                'type' => Type::nonNull(Type::float()),
                'description' => 'Transaction total balance',
            ],
            'pp1' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Transaction data',
            ],
            'pp2' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Transaction data',
            ],
            'pp3' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Transaction data',
            ],
        ];
    }
}