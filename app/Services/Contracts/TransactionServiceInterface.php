<?php

namespace App\Services\Contracts;

interface TransactionServiceInterface {
    public function getTransactionDetails($query);
}