<?php

namespace App\Services\Contracts;

interface WalletServiceInterface {
    public function getAllWallet();

    public function createWalletDetail($amount);
}