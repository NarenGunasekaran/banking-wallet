<?php

namespace App\Services;

use App\EloquentRepositories\EloquentWalletRepository;
use App\Services\Contracts\WalletServiceInterface;

class WalletService implements WalletServiceInterface
{
    public function getAllWallet(){
        $walletRepository = new EloquentWalletRepository();
        return $walletRepository->getAllWallet();
    }

    public function createWalletDetail($amount) {
        $walletRepository = new EloquentWalletRepository();
        return $walletRepository->inputWalletAmount($amount);
    }
}