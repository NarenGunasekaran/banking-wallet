<?php

namespace App\Services;

use App\EloquentRepositories\EloquentTransactionRepository;
use App\Services\Contracts\TransactionServiceInterface;
use Illuminate\Support\Facades\Log;

class TransactionService implements TransactionServiceInterface
{
    public function getTransactionDetails($query){
        $transationRepo = new EloquentTransactionRepository();
        return $transationRepo->getTransactionDetailsUsingFilters($query);
    }
}