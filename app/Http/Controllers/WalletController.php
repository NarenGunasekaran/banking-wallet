<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Contracts\WalletServiceInterface;
use Exception;
use Illuminate\Support\Facades\Log;

class WalletController extends Controller
{

    function __construct(WalletServiceInterface $walletServiceInterface)
    {
        $this->walletServiceInterface = $walletServiceInterface;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $walletList = $this->walletServiceInterface->getAllWallet();
            Log::info('walletList', [$walletList]);
            return [
                'status' => 'SUCCESS',
                'data' => $walletList
            ];
        } catch(Exception $e){
            return [
                'status' => 'FAILURE',
                'error' => $e->getMessage(),
                'code' => $e->getCode()
            ];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $amount = $request->json()->all();
            $this->walletServiceInterface->createWalletDetail($amount);
        } catch (Exception $e){
            return [
                'status' => 'FAILURE',
                'error' => $e->getMessage(),
                'code' => $e->getCode()
            ];
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
