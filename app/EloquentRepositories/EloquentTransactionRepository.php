<?php

namespace App\EloquentRepositories;

use App\Models\Transaction;
use Carbon\Carbon;
use InvalidArgumentException;
use Illuminate\Support\Facades\Cache;

class EloquentTransactionRepository
{
    public function getTransactionDetailsUsingFilters($filters){
        $order = '';
            if(isset($filters['order'])){
                $order = $filters['order'];
            }
        if(isset($filters['from']) && isset($filters['to'])) {

            if (!isset($filters['limit'])){
                throw new InvalidArgumentException('Limit is mandatory with the date filters', 400);
            }

            $cacheKey = $filters['from'] . '/' . $filters['to'] . '/' . $filters['limit'];
            
            return Cache::rememberForever($cacheKey, function () use($filters, $order){
                return Transaction::with([
                    'transaction_info'
                  ])
                  ->whereDate("created_at", ">", Carbon::parse($filters['from'])->toDateString())
                  ->whereDate("created_at", "<=", Carbon::parse($filters['to'])->toDateString())
                  ->orderBy('created_at', $order)
                  ->limit($filters['limit'])
                  ->get();
            });
            
        } else if(isset($filters['limit'])){
            return Transaction::with([
                    'transaction_info'
                ])
                ->whereDate("created_at", ">", Carbon::now()->subDays($filters['limit']))
                ->orderBy('created_at', $order)
                ->get();
        } else {
            throw new InvalidArgumentException('Filters are mandatory', 400);
        }

    }
}