<?php

namespace App\EloquentRepositories;

use App\Models\Wallet;
use App\Models\Transaction;
use InvalidArgumentException;

class EloquentWalletRepository
{
    public function getAllWallet(){
        return Wallet::all();
    }

    public function inputWalletAmount($amount){
        if (isset($amount['amount'])){
            $res = Wallet::where('user_id', '=', 1)->first();
            // dd($res['amount']);
            if($res === null){
               
                Wallet::Create(
                [
                    'user_id' => 1,
                    'amount' => $amount['amount'],
                    'type' => 'CR',
                    'currency_code' => 'INR'
                ]

                );
                $trans = Transaction::Create([
                        'user_id' => 1,
                        'type' => 'CR',
                        'amount' => $amount['amount'],
                        'status' => 'SUCCESS',
                        'reason' => 'Adding to wallet',
                        'name' => 'Naren',
                        'total_balance' => $amount['amount']             
                    ]);

                $trans->transaction_info()->create([
                    'pp1' => 'FROM',
                    'pp2' => 'HDFC Bank',
                    'pp3' => 'Transaction Details'
                ]);

            } else {
                $totalAmount = (float)$res['amount'] + (float)$amount['amount'];
                Wallet::where('user_id', '=', 1)
                    ->update([
                    'amount' => $totalAmount,
                    'type' => 'CR',
                    'currency_code' => 'INR'
                    ]);

                $trans = Transaction::Create([
                        'user_id' => 1,
                        'type' => 'CR',
                        'amount' => $amount['amount'],
                        'status' => 'SUCCESS',
                        'reason' => 'Adding to wallet',
                        'name' => 'Naren',
                        'total_balance' => $totalAmount             
                    ]);

                $trans->transaction_info()->create([
                    'pp1' => 'FROM',
                    'pp2' => 'HDFC Bank',
                    'pp3' => 'Transaction Details'
                ]);
            }

            return [
                'status' => 'SUCCESS'
            ];

        } else {
            throw new InvalidArgumentException('Amount is missing in the request body', 400);
        }
    }
}