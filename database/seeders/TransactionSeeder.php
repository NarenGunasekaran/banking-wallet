<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transaction = [
            [
                'type' => 'DR',
                'amount' => '200',
                'user_id' => '1',
                'status' => 'SUCCESS',
                'reason' => 'Shopping',
                'name' => 'Naren',
                'total_balance' => '800' ,
                'created_at' => Carbon::now()->subDays(2)->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'type' => 'DR',
                'amount' => '100',
                'user_id' => '1',
                'status' => 'SUCCESS',
                'reason' => 'Shopping',
                'name' => 'Naren',
                'total_balance' => '700' ,
                'created_at' => Carbon::now()->subDays(10)->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'type' => 'DR',
                'amount' => '300',
                'user_id' => '1',
                'status' => 'SUCCESS',
                'reason' => 'Shopping',
                'name' => 'Naren',
                'total_balance' => '400' ,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];
        DB::table('transaction')->insert($transaction);
    }
}
