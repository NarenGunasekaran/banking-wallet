<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TransactionInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactionInfo = [
            [
                'transaction_id' => '1',
                'pp1' => 'TO',
                'pp2' => 'FROM YOUR',
                'pp3' => 'transaction details',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'transaction_id' => '2',
                'pp1' => 'TO',
                'pp2' => 'FROM YOUR',
                'pp3' => 'transaction details',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'transaction_id' => '3',
                'pp1' => 'TO',
                'pp2' => 'FROM YOUR',
                'pp3' => 'transaction details',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'transaction_id' => '3',
                'pp1' => 'TO',
                'pp2' => 'FROM YOUR',
                'pp3' => 'transaction details',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];

        DB::table('transaction_info')->insert($transactionInfo);
    }
}
